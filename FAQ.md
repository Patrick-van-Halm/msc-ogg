# Ye Olde FAQ

## MSCMM is stuck while downloading FFmpeg. What should I do?

That kind of problems are often related to your Internet connection, or the problem may be on the GitLab's side.
You can either wait a little longer, or download the required files yourself [HERE](https://gitlab.com/aathlon/msc-ogg/raw/ab9cb011a283f316d56a4ce11b32558887a6fe39/Dependencies/ffpack.zip?inline=false)

## MSCMM won't start at all

Check if all required dependencies are installed. You can see them in [README.md](README.md)

## MSCMM used to work perfectly fine, but now it doesn't

That may be related to broken update, or configuration. Try to redownload the MSCMM. If that doesn't cut it, start MSCMM with "wipe" argument. That will remove all your settings.

## My game stopped working/computer blew up/dog died/I started thermonuclear war because of MSCMM

I'm sorry to hear that! Please refer to [LICENSE.md](LICENSE.md).
