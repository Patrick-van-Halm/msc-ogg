# My Summer Car Music Manager

A simple tool which makes you life easier by automatically converting, naming and managing songs in Radio and CD folders of My Summer Car.

MSC Music Manager is distributed without any warranty.

## System requirements

- Windows Vista SP2 or newer
- .NET Framework 4.6 installed

## Used third-party technologies

- [FFMpeg](https://www.ffmpeg.org) - [License](https://www.ffmpeg.org/legal.html)
- .NET Framework 4.6
- [youtube-dl](https://ytdl-org.github.io/youtube-dl/)

## License

This program is distributed under GNU General Public License v3. Feel free to use its source code as long as you mention the author and state the changes. You can also modify, share and distribute it, as long as you state changes. For more, see [LICENSE](LICENSE.md) file.

## Download

### [Download](mscmm.zip)

## Installation

Simply unzip archive anywhere you want (except for My Summer Car root folder).

## Updating

The program has automatic update function, although you can disable it in settings and update it manually by downloading the mscmm.zip from this repo.

## Images

![img1](https://i.imgur.com/0JV3HNZ.png)
![img2](https://i.imgur.com/9hcJCti.png)
![img3](https://i.imgur.com/Sl3WNQZ.png)

## Versioning

We're version this project with sequence-based identifiers versioning (2.0, then 2.1...).

## Building the source code

The program was build with Microsoft Visual Studio 2019 Community, so I think it's the best way to compile it.

## Author(s)

- [Athlon](http://twitter.com/_athl/)